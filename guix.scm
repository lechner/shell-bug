(use-modules
 (gnu packages)
 (guix build-system copy)
 (guix gexp)
 (guix git-download)
 ((guix licenses) #:prefix license:)
 (guix packages))

(define-public shell-bug
  (let ((commit "9c0482a8266a73152dbe0d3c06c5c26ec6040d18")
        (revision "0"))
    (package
      (name "shell-bug")
      (version (git-version "0.0" revision commit))
      (source (origin
                (method git-fetch)
                (uri (git-reference
                      (url "https://codeberg.org/lechner/shell-bug")
                      (commit commit)))
                (file-name (git-file-name name version))
                (sha256
                 (base32
                  "0by7rdpmasy5w16f9pkmx9x3iw9zhnk97qfas0rrbwk1vkw3khi8"))))
      (build-system copy-build-system)
      (arguments
       (list
        #:phases
        #~(modify-phases %standard-phases
            (add-before 'install 'fix-bug
              (lambda _
                (substitute* "summer"
                  (("summer") "winter")))))
        #:install-plan
        #~'(("summer" "bin/"))))
      (home-page "https://codeberg.org/lechner/shell-bug")
      (synopsis "Bug demonstration in 'guix shell'")
      (description
       "Demonstrates a bug in 'guix shell'.")
      (license license:agpl3+))))

shell-bug
